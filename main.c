/*
	Copyright (c) 2020 Christoph Willing,  Brisbane, Australia
	SPDX-License-Identifier: GPL-3.0-or-later
*/

#include <stdlib.h>
#include <string.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "datadeck_utils.h"

#define	BAUD_RATE_DEFAULT	(B19200)
#define	T40_QUIRKS_DEFAULT	(TRUE)


int
main(int argc, char *argv[])
{
    GtkBuilder *builder;
    GtkWidget  *window;
    AppWidgets *widgets = g_slice_new(AppWidgets);

    gtk_init(&argc, &argv);

    builder = gtk_builder_new_from_resource("/com/darlo/datadeck/datadeck.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    gtk_window_set_icon_name (GTK_WINDOW (window), "datadeck");

    widgets->window_main = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    widgets->reading_data_popup = GTK_WIDGET(gtk_builder_get_object(builder, "reading_data_popup"));
    widgets->preferences_popup_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "preferences_popup_dialog"));
    widgets->baud_rate_choices = GTK_WIDGET(gtk_builder_get_object(builder, "baud_rate_choices"));
    widgets->t40_quirks_choice = GTK_WIDGET(gtk_builder_get_object(builder, "t40_quirks_choice"));
    widgets->serial_ports = GTK_WIDGET(gtk_builder_get_object(builder, "serial_ports"));
    widgets->save_file = GTK_WIDGET(gtk_builder_get_object(builder, "save_file"));
    widgets->btn_open_close_port = GTK_WIDGET(gtk_builder_get_object(builder, "btn_open_close_port"));
    widgets->reading_data = GTK_WIDGET(gtk_builder_get_object(builder, "reading_data"));
    gtk_builder_connect_signals(builder, widgets);

    widgets->baud_rate = BAUD_RATE_DEFAULT;
    widgets->t40_quirks = T40_QUIRKS_DEFAULT;

    // Populate baud rate combo box
    gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(widgets->baud_rate_choices));
    for (int i=0;i<50;i++)
    {
        int entry = get_baud_rate_entry_by_index(i);
        if(entry >= 0)
        {
            gchar *entry_string = g_strdup_printf("%i", entry);
            gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widgets->baud_rate_choices), entry_string);
        }
    }
 

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();

    return 0;
}

// Main window is closed
void
on_window_main_destroy()
{
    gtk_main_quit();
}

gboolean
on_window_main_key_press_event(GtkWidget *widget, GdkEventKey *event, AppWidgets *app_widgets)
{
    GtkDialog *preferences_popup_dialog = GTK_DIALOG(app_widgets->preferences_popup_dialog);
    GtkComboBoxText *baud_rate_choices = GTK_COMBO_BOX_TEXT(app_widgets->baud_rate_choices);
    GtkCheckButton *t40_quirks_choice = GTK_CHECK_BUTTON(app_widgets->t40_quirks_choice);

    if (event->keyval == GDK_KEY_F1)
    {
        int      result;
        gboolean t40_quirks_orig;
        speed_t  baud_rate_orig;
        long speed_choice;

        // First ensure that preferences gui shows current values
        t40_quirks_orig = app_widgets->t40_quirks;
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(t40_quirks_choice), t40_quirks_orig);
        baud_rate_orig = app_widgets->baud_rate;
        gtk_combo_box_set_active(GTK_COMBO_BOX(baud_rate_choices), get_baud_rate_index(baud_rate_orig));

        // Now run the dialog box
        result = gtk_dialog_run(GTK_DIALOG(preferences_popup_dialog));
        //g_printf("pressed %d\n", result);
        switch (result)
        {
            case GTK_RESPONSE_ACCEPT:
                // Gather and apply preference settings
                //g_printf("Selected baud rate = %s\n", gtk_combo_box_text_get_active_text(baud_rate_choices));

                speed_choice = atol(gtk_combo_box_text_get_active_text(baud_rate_choices));
                if ( isValid_baud_rate_choice(speed_choice) )
                {
                    app_widgets->baud_rate = get_baud_rate(speed_choice);
                }
                app_widgets->t40_quirks = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(t40_quirks_choice));

                break;
            default:
                // Restore any preferences that were changed (but now cancelled).
                //g_printf("Unhandled resopnse\n");
                break;
        }
        gtk_widget_hide(GTK_WIDGET(preferences_popup_dialog));

        return TRUE;
    }
    return FALSE;
}

void
on_refresh_clicked(GtkButton *refresh, GtkComboBoxText *combo)
{
    char **portlist = calloc(32, sizeof(char*)); 
    char **p = portlist;

    /* Remove existing conbo box entries */
    gtk_combo_box_text_remove_all (combo);

    /* Obtain new entries */
    refresh_serial_port_list(portlist);

    /* Add them to the combobox */
    while(*p)
    {
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), *p);
        free(*p++);

        //*p++;
    }

    /* Select first in the list*/
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);
}

void
on_btn_open_close_port_toggled(GtkToggleButton *togglebutton, AppWidgets *app_widgets)
{
    char template[32];
    int temp_fd;
    FILE *f_temp_fd;
    gchar *portname;
    gboolean button_state;
    ThreadData *thread_data =  (ThreadData*)calloc(1, sizeof(ThreadData));

 
    button_state = gtk_toggle_button_get_active(togglebutton);
    if (button_state)
    {
        gtk_button_set_label((GtkButton*)togglebutton, "Close");

        portname = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(app_widgets->serial_ports));
        if (!portname) {
            g_printf("No serial port selected\n");
            gtk_toggle_button_set_active(togglebutton, FALSE);
            return;
        }
        //g_printf("Using port: %s\n", portname);

        strncpy(template, "/tmp/T40DataDeck_XXXXXX", sizeof template);
        if ( !(temp_fd = mkstemp(template)) )
        {
            perror("mkstemp()");
        }
        //g_printf("Temp file location: %s\n", template);
        if ( !(f_temp_fd=fdopen(temp_fd, "w+")) )
        {
            perror("fdopen()");
    
        }

        thread_data->app_widgets = app_widgets;
        thread_data->dest = f_temp_fd;
        thread_data->src = portname;
        thread_data->dataFileName = strdup(template);
        g_thread_new("worker", (GThreadFunc)read_serial_port_thread, thread_data);

    }
    else
    {
		if (app_widgets->fd_serial_port > -1 )
		{
			close(app_widgets->fd_serial_port);
			app_widgets->fd_serial_port = -1;
		}
		gtk_button_set_label((GtkButton*)togglebutton, "Open");
    }
}

gboolean
data_ready(ThreadData *thread_data)
{
    char *dataFileName = thread_data->dataFileName;
    AppWidgets *app_widgets = thread_data->app_widgets;
    off_t offset;

	if (app_widgets->t40_quirks)
		offset = calculate_filestart_offset(dataFileName);
	else
		offset = 0;

    // File chooser
#if GTK_MINOR_VERSION < 20
    GtkWidget *dialog;
#else
    GtkFileChooserNative *native;
#endif
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    gint res;
    GtkFileFilter *filter = gtk_file_filter_new ();

    gtk_file_filter_add_pattern (filter, "*.csv");

#if GTK_MINOR_VERSION < 20
    dialog = gtk_file_chooser_dialog_new ("Save File",
                                      GTK_WINDOW(app_widgets->window_main),
                                      action,
                                      "_Cancel",
                                      GTK_RESPONSE_CANCEL,
                                      "_Save",
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);
    chooser = GTK_FILE_CHOOSER(dialog);
#else
    native = gtk_file_chooser_native_new ("Save File",
                                          GTK_WINDOW(app_widgets->window_main),
                                          action,
                                          "_Save",
                                          "_Cancel");
    chooser = GTK_FILE_CHOOSER(native);
#endif
    gtk_file_chooser_set_filter(chooser, filter);

    gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

    //if (user_edited_a_new_document)
    gtk_file_chooser_set_current_name(chooser, "_Untitled document.csv");

#if GTK_MINOR_VERSION < 20
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
      char *filename;

      filename = gtk_file_chooser_get_filename (chooser);

      if ( (res=OSCopyFile(filename, dataFileName, offset) < 0) )
      {
          g_printf("ERROR: OSCopyFile failed (returned %d)\n", res);
      }
      g_chmod(filename, 0644);
      g_free (filename);
    }
    gtk_widget_destroy (dialog);
#else
    res = gtk_native_dialog_run (GTK_NATIVE_DIALOG (native));
    if (res == GTK_RESPONSE_ACCEPT)
    {
      char *filename;
      int res;

      filename = gtk_file_chooser_get_filename (chooser);

      if ( (res=OSCopyFile(filename, dataFileName, offset) < 0) )
      {
          g_printf("ERROR: OSCopyFile failed (returned %d)\n", res);
      }
      g_chmod(filename, 0644);
      g_free (filename);
    }
    g_object_unref (native);
#endif
    unlink(thread_data->dataFileName);
    free(thread_data->dataFileName);

    return FALSE;
}

gboolean
show_spinner(ThreadData *thread_data)
{
    AppWidgets *app_widgets = thread_data->app_widgets;
    GtkSpinner *spinner = GTK_SPINNER(app_widgets->reading_data);
    GtkWidget *popup = GTK_WIDGET(app_widgets->reading_data_popup);

    if (thread_data->run_spinner)
    {
        gtk_widget_show_all(popup);
        gtk_spinner_start(spinner);
    }
    else
    {
        gtk_spinner_stop(spinner);
        gtk_widget_hide(popup);
    }

    return FALSE;
}


/*
1. Ensure that the T40 DataDeck is turned on before connecting the USB cable.
2. Plug the USB cable in.
3. Click "Refresh" above to update the list of available USB serial ports.
4. Select the USB serial port that your T40 DadaDeck is connected to and click "Open".
5. Navigate yout T40 to the Output screen and press ENTER to begin the download.
6. When the download completes, a dialog will appear prompting you to save the file.
7. When finished click "Close" to close the USB serial port.
8. Disconnect the USB cable from the T40 before turning it off.

If you are not prompted to save the file. check that you have selected the correct
USB port. Also, try clicking "Close" and then "Open" again.";
*/

