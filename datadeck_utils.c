/*
        Copyright (c) 2020 Christoph Willing,  Brisbane, Australia
        SPDX-License-Identifier: GPL-3.0-or-later
*/
#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#if defined(__APPLE__) || defined(__FreeBSD__)
#include <copyfile.h>
#else
#include <sys/sendfile.h>
#endif
#include <dirent.h>

#include "datadeck_utils.h"


/* From: https://stackoverflow.com/questions/57152937/canonical-mode-linux-serial-port/57155531 */
int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= CLOCAL | CREAD;
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    tty.c_lflag |= ICANON | ISIG;  /* canonical input */
    tty.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

    tty.c_iflag &= ~IGNCR;  /* preserve carriage return */
    tty.c_iflag &= ~INPCK;
    tty.c_iflag &= ~(INLCR | ICRNL | IUCLC | IMAXBEL);
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);   /* no SW flowcontrol */

    tty.c_oflag &= ~OPOST;

    tty.c_cc[VEOL] = 0;
    tty.c_cc[VEOL2] = 0;
    tty.c_cc[VEOF] = 0x04;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void
refresh_serial_port_list(char **ports)
{
    char **p = ports;

    char *filter = "ttyUSB";
    DIR *dir;
    struct dirent *ent;

    if ((dir = opendir ("/dev")) != NULL)
    {
      while ((ent = readdir (dir)) != NULL)
      {
        if ( strncmp(ent->d_name, filter, strlen(filter)) == 0 )
        {
            //printf("Found: %s\n", ent->d_name);
            *p++ = strdup(ent->d_name);
        }
      }
      closedir (dir);

    }
    else
    {
      /* could not open directory */
      perror ("");
    }
}


/*
The output of the T40 sometimes has spurious \r characters
at the beginning of the file. We don't want these in the
final result, rather we want to start the final result with
a date entry e.g.
    DATE,05/10/20
Here we calculate and return the number of unwanted bytes
(or -1 on error)
*/
int
calculate_filestart_offset(char *source)
{
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int offset;

    fp = fopen(source, "r");
    if (fp == NULL )
    {
        offset = -1;
    }
    else 
    {
        read = getline(&line, &len, fp);
        if (read > 4 )    /* the word "DATE" so no redundant chars */
            offset = 0;
        else
            offset = read;
    }
    fclose(fp);

    return offset;
}

/* From https://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c */
int
OSCopyFile(const char* destination, const char* source, off_t offset)
{
    int input, output;
    //printf("Copying from %s to %s\n", source, destination);

    if ((input = open(source, O_RDONLY)) == -1)
    {
        return -1;
    }
    if ((output = creat(destination, 0660)) == -1)
    {
        close(input);
        return -1;
    }

    //Here we use kernel-space copying for performance reasons
#if defined(__APPLE__) || defined(__FreeBSD__)
    //fcopyfile works on FreeBSD and OS X 10.5+ 
    int result = fcopyfile(input, output, 0, COPYFILE_ALL);
#else
    //sendfile will work with non-socket output (i.e. regular file) on Linux 2.6.33+
    off_t bytesCopied = offset;  /* Avoid unwanted \r at beginning of file */
    struct stat fileinfo = {0};
    fstat(input, &fileinfo);
    int result = sendfile(output, input, &bytesCopied, fileinfo.st_size);
#endif

    close(input);
    close(output);

    return result;
}

void
read_serial_port_thread(gpointer data)
{
    ThreadData *thread_data = (ThreadData*)data;
    FILE *dest = thread_data->dest;
    char *src = thread_data->src;
    int fd;
    fd_set rfds;
    struct timeval tv;
    int retval;
    char portloc[32]={'\0'};
    gboolean spinner_running = FALSE;

    strcat(portloc, "/dev/");
    strcat(portloc, src);

    fd = open(portloc, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        printf("Error opening %s: %s\n", portloc, strerror(errno));
        return;
    }
    /*baudrate 19200, 8 bits, no parity, 1 stop bit */
    //set_interface_attribs(fd, B19200);
    set_interface_attribs(fd, thread_data->app_widgets->baud_rate);

    tcdrain(fd);    /* delay for output */


    /* simple canonical input */
    do
    {
        unsigned char buf[83];
        int rdlen;
        int maxfd;

        rdlen = read(fd, buf, sizeof(buf) - 1);

    if (!spinner_running)
    {
        spinner_running=TRUE;
        thread_data->run_spinner = spinner_running;
        g_idle_add((GSourceFunc)show_spinner, thread_data);
    }
        if (rdlen > 0)
        {
            buf[rdlen] = 0;
            //printf("%s", buf);
            fprintf(dest, "%s", buf);
        }
        else if (rdlen < 0)
        {
            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
        }
        else
        {  /* rdlen == 0 */
            printf("Nothing read. EOF?\n");
        }               

        /* Check whether there's more to read within some timeout, otherwise assume we're done */
        maxfd = fd + 1;
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);

        /* Wait for up to one seconds */
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        retval = select(maxfd, &rfds, NULL, NULL, &tv);
        if (retval == -1 )
        {
            perror("select()");
        }
        else if (retval)
        {
            /* Data should be available (check first with FD_ISSET ?) */
            continue;
        }
        else
        {
            /* No data within timeout period - assume we're done */
            fprintf(dest, "\r");
            break;
        }

    } while (1);
    fclose(dest);

    spinner_running = FALSE;
    thread_data->run_spinner = spinner_running;
    thread_data->app_widgets->fd_serial_port = fd;
    g_idle_add((GSourceFunc)show_spinner, thread_data);
    g_idle_add((GSourceFunc)data_ready, thread_data);
}

struct
{
  long rawrate;
  speed_t termiosrate;
} conversiontable[] =
{
  {0, B0},
  {50, B50},
  {75, B75},
  {110, B110},
  {134, B134},
  {150, B150},
  {200, B200},
  {300, B300},
  {600, B600},
  {1200, B1200},
  {1800, B1800},
  {2400, B2400},
  {4800, B4800},
  {9600, B9600},
  {19200, B19200},
  {38400, B38400},
  // Remainder are non-POSIX apparently (from <bits/termios-baud.h>)
  {57600, B57600},
  {115200, B115200},
  {230400, B230400},
  {460800, B460800},
  {500000, B500000},
  {576000, B576000},
  {921600, B921600},
  {1000000, B1000000},
  {1152000, B1152000},
  {1500000, B1500000},
  {2000000, B2000000},
  {2500000, B2500000},
  {3000000, B3000000},
  {3500000, B3500000},
  {4000000, B4000000}
};


speed_t
get_baud_rate(long rawrate)
{
    for (int i=0; i<sizeof(conversiontable) / sizeof(conversiontable[0]); i++)
    {
        if (conversiontable[i].rawrate == rawrate)
        {
            return conversiontable[i].termiosrate;
        }
    }
    // Error
    return conversiontable[0].termiosrate;
}

int
get_baud_rate_index(speed_t termiosrate)
{
    for (int i=0; i<sizeof(conversiontable) / sizeof(conversiontable[0]); i++)
    {
        if (conversiontable[i].termiosrate == termiosrate)
        {
            return i;
        }
    }
    // Error
    return -1;
}

long
get_baud_rate_entry_by_index(int index)
{
    if ((index > -1) && (index < sizeof(conversiontable) / sizeof(conversiontable[0]) ))
    {
        return conversiontable[index].rawrate;
    }
    return -1;
}

gboolean
isValid_baud_rate(speed_t termiosrate)
{
    for (int i=0; i<sizeof(conversiontable) / sizeof(conversiontable[0]); i++)
    {
        if (conversiontable[i].termiosrate == termiosrate)
        {
            return TRUE;
        }
    }
    return FALSE;
}
gboolean
isValid_baud_rate_choice(long rawrate)
{
    for (int i=0; i<sizeof(conversiontable) / sizeof(conversiontable[0]); i++)
    {
        if (conversiontable[i].rawrate == rawrate)
        {
            return TRUE;
        }
    }
    return FALSE;
}

