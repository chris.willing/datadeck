/*
        Copyright (c) 2020 Christoph Willing,  Brisbane, Australia
        SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef DATADECK_UTILS_H
#define DATADECK_UTILS_H

#include <glib/gprintf.h>
#include <gtk/gtk.h>
#include <termios.h>

typedef struct {
    GtkWidget *window_main;
    GtkWidget *reading_data_popup;
    GtkWidget *preferences_popup_dialog;
    GtkWidget *baud_rate_choices;
    GtkWidget *t40_quirks_choice;
    GtkWidget *serial_ports;
    GtkWidget *save_file;
    GtkWidget *btn_open_close_port;
    GtkWidget *reading_data;
    speed_t   baud_rate;
    gboolean  t40_quirks;

    /*	Not really widget related but facilitates
		closing serial port from thread other than
		the one it was created in
    */
    int       fd_serial_port;
} AppWidgets;

typedef struct {
    AppWidgets *app_widgets;
    char *dataFileName;
    FILE *dest;
    char *src;
    gboolean run_spinner;
} ThreadData;

int read_serial_port(FILE *, char*);
void read_serial_port_thread(gpointer data);
int calculate_filestart_offset(char*);
int OSCopyFile(const char*, const char*, off_t);

void refresh_serial_port_list(char**);

gboolean data_ready(ThreadData*);
gboolean show_spinner(ThreadData*);
speed_t get_baud_rate(long);
int get_baud_rate_index(speed_t);
long get_baud_rate_entry_by_index(int);
gboolean isValid_baud_rate(speed_t);
gboolean isValid_baud_rate_choice(long);


#endif	/* DATADECK_UTILS_H */

