TARGET=datadeck
DESTDIR ?= /

CC=gcc
DEBUG=-g
OPT=-O0
WARN=-Wall
PTHREAD=-pthread

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) -pipe -std=gnu99

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

LD=gcc
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic

PKGCONFIG=$(shell which pkg-config)
GLIB_COMPILE_RESOURCES = $(shell $(PKGCONFIG) --variable=glib_compile_resources gio-2.0)
RESOURCES = $(shell $(GLIB_COMPILE_RESOURCES) --sourcedir=. --generate-dependencies datadeck.gresource.xml)

OBJS=    main.o datadeck_utils.o datadeck-resources.o

$(TARGET): $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: main.c datadeck_utils.h
	$(CC) -c $(CCFLAGS) main.c $(GTKLIB) -o main.o
    
datadeck_utils.o: datadeck_utils.c datadeck_utils.c
	$(CC) -c $(CCFLAGS) datadeck_utils.c $(GTKLIB) -o datadeck_utils.o

datadeck-resources.o: datadeck.gresource.xml $(RESOURCES) datadeck-resources.c
	$(CC) -c $(CCFLAGS) datadeck-resources.c $(GTKLIB) -o datadeck-resources.o

datadeck-resources.c: datadeck.gresource.xml $(RESOURCES)
	$(GLIB_COMPILE_RESOURCES) datadeck.gresource.xml --target=$@ --sourcedir=. --generate-source


install: $(TARGET)
	echo $(DESTDIR)
	install -d $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/share/applications
	install -d $(DESTDIR)/usr/share/icons/hicolor/scalable/apps
	install -m 0755 datadeck $(DESTDIR)/usr/bin
	install -m 0644 datadeck.desktop $(DESTDIR)/usr/share/applications/$(TARGET).desktop
	install -m 0644 images/ttm.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/$(TARGET).svg

clean:
	rm -f *.o $(TARGET) datadeck.gresource datadeck-resources.c


